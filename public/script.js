const socket = io("/");
const videoGrid = document.getElementById("video-grid");
const myVideo = document.createElement("video");
const showChat = document.querySelector("#showChat");
const backBtn = document.querySelector(".header__back");
myVideo.muted = true;

backBtn.addEventListener("click", () => {
    document.querySelector(".main__left").style.display = "flex";
    document.querySelector(".main__left").style.flex = "1";
    document.querySelector(".main__right").style.display = "none";
    document.querySelector(".header__back").style.display = "none";
});

showChat.addEventListener("click", () => {
    document.querySelector(".main__right").style.display = "flex";
    document.querySelector(".main__right").style.flex = "1";
    document.querySelector(".main__left").style.display = "none";
    document.querySelector(".header__back").style.display = "block";
});

const user = prompt("Enter your name");

const peer = new Peer(undefined, {
    path: "/peerjs",
    host: "/",
    port: PORT,
    debug: true,
});

console.log(PORT)

let myVideoStream;
navigator.mediaDevices.getUserMedia({
    audio: true,
    video: true,
}).then((stream) => {
    myVideoStream = stream;
    addVideoStream(myVideo, stream, userIdLocal);

    peer.on("call", (call) => {
        call.answer(stream);
        const video = document.createElement("video");
        call.on("stream", (userVideoStream) => {
            addVideoStream(video, userVideoStream);
        });
    });

    socket.on("user-connected", async (userId) => {
        await new Promise(r => setTimeout(r, 1000));
        connectToNewUser(userId, stream);
    });
});

const connectToNewUser = (userId, stream) => {
    const call = peer.call(userId, stream);
    const video = document.createElement("video");
    console.log('connected to new user', userId)
    call.on("stream", (userVideoStream) => {
        console.log('connect to new user', userId)
        addVideoStream(video, userVideoStream, userId);
    });
};

let userIdLocal;
peer.on("open", (id) => {
    userIdLocal = id;
    socket.emit("join-room", ROOM_ID, id, user);
});

window.addEventListener("beforeunload", function() {
    socket.emit("leave-room", ROOM_ID, userIdLocal);
});

peer.on('close', () => {
    socket.emit("leave-room", ROOM_ID, userIdLocal);
});

const addVideoStream = (video, stream, id) => {
    video.srcObject = stream;
    console.log(id)
    if (id) video.id = id;
    video.addEventListener("loadedmetadata", () => {
        video.play();
        videoGrid.append(video);
    });
};

let text = document.querySelector("#chat_message");
let send = document.getElementById("send");
let messages = document.querySelector(".messages");

send.addEventListener("click", (e) => {
    if (text.value.length !== 0) {
        socket.emit("message", text.value);
        text.value = "";
    }
});

text.addEventListener("keydown", (e) => {
    if (e.key === "Enter" && text.value.length !== 0) {
        socket.emit("message", text.value);
        text.value = "";
    }
});

const startWhisper = document.querySelector("#startWhisper");
const inviteButton = document.querySelector("#inviteButton");
const muteButton = document.querySelector("#muteButton");
const stopVideo = document.querySelector("#stopVideo");

startWhisper.addEventListener("click", (e) => {
    console.log("button gedrück alter");
    socket.emit("startWhisper");
});

muteButton.addEventListener("click", () => {
    const enabled = myVideoStream.getAudioTracks()[0].enabled;
    if (enabled) {
        myVideoStream.getAudioTracks()[0].enabled = false;
        html = `<i class="fas fa-microphone-slash"></i>`;
        muteButton.classList.toggle("background__red");
        muteButton.innerHTML = html;
    } else {
        myVideoStream.getAudioTracks()[0].enabled = true;
        html = `<i class="fas fa-microphone"></i>`;
        muteButton.classList.toggle("background__red");
        muteButton.innerHTML = html;
    }
});

stopVideo.addEventListener("click", () => {
    const enabled = myVideoStream.getVideoTracks()[0].enabled;
    if (enabled) {
        myVideoStream.getVideoTracks()[0].enabled = false;
        html = `<i class="fas fa-video-slash"></i>`;
        stopVideo.classList.toggle("background__red");
        stopVideo.innerHTML = html;
    } else {
        myVideoStream.getVideoTracks()[0].enabled = true;
        html = `<i class="fas fa-video"></i>`;
        stopVideo.classList.toggle("background__red");
        stopVideo.innerHTML = html;
    }
});

inviteButton.addEventListener("click", (e) => {
    prompt(
        "Copy this link and send it to people you want to meet with",
        window.location.href
    );
});

socket.on("createMessage", (message, userName) => {
    messages.innerHTML =
        messages.innerHTML +
        `<div class="message">
        <b><i class="far fa-user-circle"></i> <span> ${
            userName === user ? "me" : userName
        }</span> </b>
        <span>${message}</span>
    </div>`;
});

socket.on("destroyUser", (id) => {
    console.log(userIdLocal, id)
    const video = document.getElementById(id)
    console.log('remove', video)
    if (video) video.remove()
});

function enableVideo() {
    myVideoStream.getVideoTracks()[0].enabled = true;
    html = `<i class="fas fa-video"></i>`;
    stopVideo.classList.toggle("background__red");
    stopVideo.innerHTML = html;
}

function disableVideo() {
    myVideoStream.getVideoTracks()[0].enabled = false;
    html = `<i class="fas fa-video-slash"></i>`;
    stopVideo.classList.toggle("background__red");
    stopVideo.innerHTML = html;
}

function enableAudio() {
    myVideoStream.getAudioTracks()[0].enabled = true;
    html = `<i class="fas fa-microphone"></i>`;
    muteButton.classList.toggle("background__red");
    muteButton.innerHTML = html;
}

function diasbleAudio() {
    myVideoStream.getAudioTracks()[0].enabled = false;
    html = `<i class="fas fa-microphone-slash"></i>`;
    muteButton.classList.toggle("background__red");
    muteButton.innerHTML = html;
}

socket.on("startGame", (currentPlayers) => {
    if(currentPlayers.indexOf(userIdLocal) == 0) {
        enableVideo();
        enableAudio();
    }
    else if(currentPlayers.indexOf(userIdLocal) == 1) {
        diasbleAudio();
        enableVideo();
    }
    else {
        disableVideo();
        diasbleAudio();
    }
});

socket.on("nextRound", (currentPlayers) => {
    if(currentPlayers.indexOf(userIdLocal) == 0) {
        enableVideo();
        enableAudio();
    }
    else if(currentPlayers.indexOf(userIdLocal) == 1) {
        diasbleAudio();
        enableVideo();
    }
    else {
        disableVideo();
        diasbleAudio();
    }
});

socket.on("endGame", () => {
    enableVideo();
    enableAudio();
});
