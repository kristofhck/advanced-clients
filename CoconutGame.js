class CoconutGame {
  static allInstances = [];
  constructor(roomid) {
    
    this.users = [];
    this.roomid = roomid;
    this.ongoingRound = false;
    this.usersInRound = [];
    this.playerArr = [];
  }

  static get(uuid) {
    for(var i=0; i<CoconutGame.allInstances.length; i++) {
      let game = CoconutGame.allInstances[i];
      if(game.roomid == uuid) {
        return game;
      }
    }
    let newGame = new CoconutGame(uuid);
    CoconutGame.allInstances.push(newGame);
    return newGame;
  }

  addUser(user) {
    this.users.push(user);
  }

  removeUser(usr) {
    this.users = this.users.filter((user) => user !== usr);
  }

  start(userId) {
    this.playerArr[0] = userId;
    this.ongoingRound = true;
    this.usersInRound = [...this.users];

    var index = this.usersInRound.indexOf(userId);
    if (index > -1) {
      this.usersInRound.splice(index, 1);
    }
    
    if(this.usersInRound.length > 0) {
      this.playerArr[1] = this.usersInRound.pop();
    } else {
      return false;
    }

    return this.playerArr;
  }
  
  next() {
    this.playerArr[0] = this.playerArr[1];
    this.ongoingRound = true;

    if(this.usersInRound.length > 0) {
      this.playerArr[1] = this.usersInRound.pop();
    } else {
      return false;
    }

    return this.playerArr;
  }
}

module.exports = CoconutGame;