# coconut mail

Taking turns, a player thinks of a word and whispers it into the ear of the next player via WebRTC. The next player whispers the understood word into the ear of the next player. This continues until the word reaches the last player in the round. Then all clients are unmuted and the word is shouted out loud by the last player.

## How to run the project?

1. Clone this repository in your local system.
2. Open the command prompt from your project directory and run the command `npm start`.
3. Go to your browser and type `http://127.0.0.1:3030/` in the address bar.
4. Hurray! That's it.
