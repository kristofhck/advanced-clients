const express = require("express");
const app = express();
const server = require("http").Server(app);
const { v4: uuidv4 } = require("uuid");
const CoconutGame = require('./CoconutGame.js');
app.set('view engine', 'ejs')

const io = require("socket.io")(server, {
    cors: {
        origin: '*'
    }
});
const { ExpressPeerServer } = require("peer");
const peerServer = ExpressPeerServer(server, {
    debug: true,
});

app.use("/peerjs", peerServer);
app.use(express.static('public'));

app.get("/", (req, res) => {
  res.redirect(`/${uuidv4()}`);
});

app.get("/:room", (req, res) => {
  res.render("room", { port: process.env.PORT ? 443 : 3030, roomId: req.params.room });
});

io.on("connection", (socket) => {
    socket.on("join-room", (roomId, userId, userName) => {
        let game = CoconutGame.get(roomId);
        game.addUser(userId);
        socket.join(roomId);
        socket.to(roomId).broadcast.emit("user-connected", userId);
        socket.on("message", (message) => {
            io.to(roomId).emit("createMessage", message, userName);
        });

        socket.on("startWhisper", async () => {
            let currentPlayers = game.start(userId);
            if(currentPlayers) {
                io.to(roomId).emit("startGame", currentPlayers, userId);
            }
            console.log("start");
            console.log(game);
            await new Promise(r => setTimeout(r, 10000));

            while(true) {
                currentPlayers = game.next();

                if(currentPlayers) {
                    console.log("next");
                    console.log(game);
                    io.to(roomId).emit("nextRound", currentPlayers, userId);
                    await new Promise(r => setTimeout(r, 10000));
                } else {
                    break;
                }
            }
            io.to(roomId).emit("endGame");
        });
    });

    socket.on("leave-room", (roomId, userId) => {
        let game = CoconutGame.get(roomId);
        game.removeUser(userId);
        io.to(roomId).emit("destroyUser", userId)
    });
});
server.listen(process.env.PORT || 3030);


